import { formatMoney } from '@shopify/theme-currency';

declare let theme: any;

/**
 * Format money from Shopify theo money_format
 */

const hugMoneyFormat = (value: number | string): string => {
  const currencyFormat = formatMoney(Number(value), theme.moneyFormat);
  const currencyDecimal = currencyFormat.slice(currencyFormat.indexOf(','));
  if (currencyDecimal === ',00') {
    return currencyFormat.slice(0, currencyFormat.indexOf(','));
  }
  return currencyFormat;
};

export default hugMoneyFormat;
