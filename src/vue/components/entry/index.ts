import { onSectionSelected } from 'Helpers/sections';
import PageAlles from './alles/PageAlles.vue';
import CartDrawer from './cart/CartDrawer.vue';
import CartItemCount from '../globals/CartItemCount.vue';
import ProductReview from './product-review/ProductReview.vue';
import ModalFaq from './faq/ModalFaq.vue';
import './alles/AllesContainer';

onSectionSelected('#page-alles', () => {
  const pageAlles = new PageAlles().$mount('#page-alles');
});

onSectionSelected('#product-review', () => {
  const productReview = new ProductReview().$mount('#product-review');
});

const cartDrawer = new CartDrawer().$mount('#cart-drawer');

onSectionSelected('#cart-item-count', () => {
  const cartItemCount = new CartItemCount().$mount('#cart-item-count');
});

onSectionSelected('#selectWrapper', () => {
  const modalFaq = new ModalFaq().$mount('#selectWrapper');
});
