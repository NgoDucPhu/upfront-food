import Vue from 'vue';
import Vuex from 'vuex';
import CartStore from './modules/cart';
import CollectionStore from './modules/collection';
import ReviewStore from './modules/productReview';
import ToastStore from './modules/toast';

Vue.use(Vuex);
const store = new Vuex.Store({
  modules: {
    CartStore,
    CollectionStore,
    ReviewStore,
    ToastStore,
  },
});

export default store;
