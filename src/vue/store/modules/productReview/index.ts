import {
  Bottomline,
  CRFS,
  Pagination,
  Reviews,
  ReviewsEntity,
} from 'Types/review';
import {
  Action,
  Module,
  Mutation,
  VuexModule,
} from 'vuex-module-decorators';

enum MutationType {
  setCRFS = 'SET_CRFS',
  setIsFilterLoading = 'SET_IS_FILTER_LOADING',
  setReviewList = 'SET_REVIEW_LIST',
  setBottomLine = 'SET_BOTTOM_LINE',
  setDataFilter = 'SET_DATA_FILTER',
  setPagination = 'SET_PAGINATION',
  setProductId = 'SET_PRODUCT_ID',
}

@Module({
  namespaced: true,
})
export default class ReviewStore extends VuexModule {
  private crfs: CRFS[] = [];

  private dataFilter = {} as any;

  private isFilterLoading = false;

  private reviewList: ReviewsEntity[] = [] as ReviewsEntity[];

  private pagination: Pagination = {} as Pagination;

  private bottomLine: Bottomline = {} as Bottomline;

  private productId: string;

  get getCrfs(): CRFS[] {
    return this.crfs;
  }

  get getIsFilterLoading(): boolean {
    return this.isFilterLoading;
  }

  get getReviewList(): ReviewsEntity[] {
    return this.reviewList;
  }

  get getBottomLine(): Bottomline {
    return this.bottomLine;
  }

  get getDataFilter(): any {
    return this.dataFilter;
  }

  get getPagination(): Pagination {
    return this.pagination;
  }

  get getProductId(): string {
    return this.productId;
  }

  @Mutation
  [MutationType.setCRFS](crf: CRFS): void {
    const i = this.crfs.findIndex((crfT: CRFS) => crfT.question_id === crf.question_id);
    if (i > -1) {
      if (crf.answers[0] !== '') {
        this.crfs[i] = crf;
      } else {
        this.crfs.splice(i, 1);
      }
    } else this.crfs.push(crf);
  }

  @Mutation
  [MutationType.setIsFilterLoading](isFilterLoading: boolean): void {
    this.isFilterLoading = isFilterLoading;
  }

  @Mutation
  [MutationType.setReviewList](reviewList: ReviewsEntity[]): void {
    this.reviewList = reviewList;
  }

  @Mutation
  [MutationType.setBottomLine](bottomLine: Bottomline): void {
    this.bottomLine = bottomLine;
  }

  @Mutation
  [MutationType.setDataFilter](newData: any): void {
    if (newData.sortings && Object.keys(newData.sortings[0]).length === 0) {
      delete this.dataFilter.sortings;
    } else {
      this.dataFilter = { ...this.dataFilter, ...newData };
    }
  }

  @Mutation
  [MutationType.setPagination](newPagination: Pagination): void {
    this.pagination = newPagination;
  }

  @Mutation
  [MutationType.setProductId](productId: string): void {
    this.productId = productId;
  }

  @Action({ rawError: false })
  public async filterAPI(data: any = this.dataFilter): Promise<void> {
    if (data.domain_key) {
      this.context.commit(MutationType.setIsFilterLoading, true);
      const res = await fetch(
        'https://api.yotpo.com/v1/reviews/TO1vyGlOUMHn44dQPyVyRhpmCox6vCxnKg4HuPMD/filter.json',
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        },
      );
      const reviewFilter: Reviews = await res.json();
      this.context.commit(MutationType.setIsFilterLoading, false);
      if (reviewFilter.status.code === 200) {
        this.context.commit(MutationType.setReviewList, reviewFilter.response.reviews);
        this.context.commit(MutationType.setPagination, reviewFilter.response.pagination);
      }
    }
  }

  @Action({ rawError: false })
  public updateCRFS(crf: CRFS): void {
    this.context.commit(MutationType.setCRFS, crf);
    const { crfs } = this;
    this.context.dispatch('updateDataFilter', { crfs, page: 1 });
  }

  @Action({ rawError: false })
  public updateDataFilter(newData: any): void {
    this.context.commit(MutationType.setDataFilter, newData);
    this.context.dispatch('filterAPI', this.dataFilter);
  }

  @Action({ rawError: false })
  public updateProductId(productId: number): void {
    if (productId === 6608041639998 || productId === 6609618894910) {
      productId = 6575748644926;
    }
    this.context.commit(MutationType.setDataFilter, { domain_key: productId });
    this.context.dispatch('filterAPI', this.dataFilter);
  }

  // Get bottomline
  @Action
  public async updateBottomLine(productId: number): Promise<void> {
    let newID: number;
    if (productId === 6608041639998 || productId === 6609618894910) {
      newID = 6575748644926;
    } else newID = productId;
    const res = await fetch(`https://api.yotpo.com/v1/widget/TO1vyGlOUMHn44dQPyVyRhpmCox6vCxnKg4HuPMD/products/${newID}/reviews.json`);
    const bottomLine = await res.json();
    this.context.commit(MutationType.setBottomLine, bottomLine.response.bottomline);
  }
}
