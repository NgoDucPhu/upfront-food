import {
  Action,
  Module,
  Mutation,
  VuexModule,
} from 'vuex-module-decorators';
import { IToastMessage } from 'Types/alles';

enum MutationType {
  setContentToast = 'SET_CONTENT_TOAST',
}

declare const Toastify: any;
const isInViewPort = (el: HTMLElement): boolean => {
  const rect = el.getBoundingClientRect();
  return (
    rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

const getYPositionToast = (): number => {
  const topSlider = document.getElementById('shopify-section-header-slider-bar') as HTMLElement;
  const topMenu = document.getElementById('shopify-section-header') as HTMLElement;

  let yPosition = 0;
  if (isInViewPort(topSlider) && isInViewPort(topMenu)) {
    yPosition = topSlider.offsetHeight + topMenu.offsetHeight;
  } else {
    yPosition = topMenu.offsetHeight;
  }

  return (yPosition + 5);
};

const getXPositionToast = (): number => {
  const topMenuContainer = document.getElementById('headerContainer') as HTMLElement;
  const spaceToRight = topMenuContainer.getBoundingClientRect().x;

  return spaceToRight;
};

@Module({
  namespaced: true,
})
export default class ToastStore extends VuexModule {
  contentToast: IToastMessage;

  get getContentToast(): IToastMessage {
    return this.contentToast;
  }

  @Mutation
  [MutationType.setContentToast](payload: IToastMessage): void {
    this.contentToast = payload;
  }

  @Action
  toggleToast(payload: IToastMessage): void {
    const quantityElement = payload.quantity ? `(${payload.quantity})` : '';
    Toastify({
      text: `
      <div>
        <div class="toast__title">Toegevoegd aan winkelwagen:</div>
        <strong>
          <span>${payload.title}</span>&nbsp;
          <span>${quantityElement}</span>
        </strong>
        <div class="toast__quantity"> - ${payload.decscription}</div>
      </div>
      `,
      className: 'xo-toast-msg xo-toast-msg--is-success xo-toast-msg--cart',
      backgroundColor: '#242424',
      duration: 3000,
      close: true,
      offset: {
        x: getXPositionToast(),
        y: getYPositionToast(),
      },
      onClick: () => {
        const closeEl = document.querySelector('.toast-close') as HTMLElement;
        closeEl.click();
      }
    }).showToast();

    this.context.commit(MutationType.setContentToast, payload);
  }
}
