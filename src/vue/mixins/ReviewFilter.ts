import Vue from 'vue';
import Component from 'vue-class-component';

@Component
export default class ReviewFilter extends Vue {
  listsOption = {
    Geslacht: {
      id: 55638,
      options: ['Man', 'Vrouw'],
    },
    Leeftijd: {
      id: 55639,
      options: ['17 - 24', '25 - 30', '31 - 40', '41 - 50', '51 - 60', '60+'],
    },
    Activiteit: {
      id: 55636,
      options: ['Heel actief', 'Gemiddeld', 'Niet zo actief'],
    },
  };

  get listOption(): any {
    return this.listsOption;
  }
}
