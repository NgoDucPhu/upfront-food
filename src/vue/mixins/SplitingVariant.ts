import { VariantsEntity } from 'Types/product-information/information';
import Vue from 'vue';
import Component from 'vue-class-component';

export const getMetaVariant = (choosenVariant: VariantsEntity): string => {
  // Only variant with default title
  if (choosenVariant.title === 'Default Title') return '';
  const res = (choosenVariant.title || '').split(' ');
  let metaVariant = '';
  if (res.length > 1) {
    (res.slice(0, -1)).forEach((title: string) => {
      metaVariant += `${title} `;
    });
  } else {
    [metaVariant] = res;
  }

  return metaVariant || '';
};

export const getQuantityVariant = (choosenVariant: VariantsEntity): string => {
  // Only variant with default title
  if (choosenVariant.title === 'Default Title') return '';
  const res = (choosenVariant.title || '').split(' ');
  const quantityVariant = (res.length > 1) ? res[Number(res.length - 1)] : '';
  return quantityVariant || '';
};

/**
 * Spliting variant title || count package variant
 */
@Component
export default class SplitingVariant extends Vue {
  hoverVariant: VariantsEntity = {} as VariantsEntity;

  get metaVariant(): string {
    return getMetaVariant(this.hoverVariant);
  }

  get quantityVariant(): string {
    return getQuantityVariant(this.hoverVariant);
  }
}
