export interface ProductInformation {
  generalInformations: GeneralInformations;
  variants: (VariantsEntity)[];
  id: number;
  mix?: (MixEntity)[] | null;
  media: Media[];
  totalQuantity: number;
  boekBackground: string,
  reviewBackground: string,
  boek: Banner,
  faqs: Banner,
  review: Banner,
  mixVariants: MixVariant[]
}

export interface Banner {
  title: string,
  color: string,
  bgDesktop: string,
  bgMobile: string,
}

export interface Media {
  id: string;
  mediaContentType: string;
  image?: {
    altText: string,
    originalSrc: string;
  };
  originalSource?: {
    format: string;
    mimeType: string;
    url: string;
    height: number;
    width: number;
  };
  preview: {
    image: {
      altText: string;
      id: string;
      originalSrc: string;
    }
  }
}
export interface GeneralInformations {
  descriptions: Description[];
  packages: Packages;
  finances: Finances;
  progress: Progress;
  bloodSugar: BloodSugar;
  icon: string;
  faq: FAQ[];
}

export interface BloodSugar {
  explain: string,
  explainTitle: string,
  xTitle: string,
  yTitle: string,
  items: [{
    title: string,
    color: string,
    data: [{
      x: number,
      y: number
    }]
  }],
  labelDisplay: string,
  isDisableLabel: boolean,
}

export interface MixVariant {
  _id: string,
  image: string,
  title2: string,
  link: string,
  isHide: boolean,
}

export interface Description {
  items: string[];
  title: string;
  type: 'desc' | 'accordion' | 'list';
  value?: string;
}

export interface Packages {
  listPackages: (ListPackagesEntityOrIngredientsEntity)[] | [];
  labelDisplay: string;
  isDisableLabel: boolean;
}
export interface ListPackagesEntityOrIngredientsEntity {
  label: string;
  value: string;
  img?: string;
  percent: number;
}

export interface FAQ {
  label: string,
  value: string
}
export interface Finances {
  financialDescription: string;
  financialDetails: (FinancialDetailsEntityOrNutritionalDetailEntity)[] | [];
  labelDisplay: string;
  isDisableLabel: boolean;
}
export interface FinancialDetailsEntityOrNutritionalDetailEntity {
  typeText: string;
  label: string;
  value: string;
}
export interface Progress {
  progressions: (ProgressionsEntity)[] | [];
  labelDisplay: string;
  isDisableLabel: boolean;
}
export interface ProgressionsEntity {
  done: boolean;
  image: string;
  subValue: string;
  label: string;
  value: string;
}
export interface VariantsEntity {
  ingredientInformations: IngredientInformations;
  nutritionInformations: NutritionInformations;
  nutritionsInfo: NutritionInformations;
  id: number;
  title: string;
  hide: boolean;
  image: ImageOrFeaturedImage;
  media: Media[];
  groups: GroupVariant[];
  available: boolean;
  quantity: number;
  price: number;
  // Image chinh cua variant day, nam dau tien trong mang Media[]
  featured_image: string;
  accordion: Accordion[];
  goedOmTeWetenInfomations: GoedOmTeWetenInfomations;
  defaultProductPage: number;
}

export interface Accordion {
  desc: string,
  title: string
}

export interface GoedOmTeWetenInfomations {
  labelDisplay: string,
  isDisableLabel: boolean,
  goedOmTeWeten: GoedOmTeWeten[]
}

export interface GoedOmTeWeten {
  items: string,
  icon: string,
  title: string
}
export interface IngredientInformations {
  ingredients: (ListPackagesEntityOrIngredientsEntity)[] | [];
  labelDisplay: string;
  isDisableLabel: boolean;
}
export interface NutritionInformations {
  nutritions: (NutritionsEntity)[] | [];
  labelDisplay: string;
  isDisableLabel: boolean;
}
export interface NutritionsEntity {
  nutritionalDetail: (NutritionDetail)[] | [];
  title: string;
}

export interface NutritionDetail {
  label: string;
  value: string;
  color: string;
  unit: string;
  type: 'settings' | 'none' | 'customize';
  className: string;
  subNutrition: {
    label: string;
    value: string;
    color: string;
    unit: string;
    type: 'settings' | 'none' | 'customize';
    className: string;
  },
  enableSub: boolean;
}
export interface ImageOrFeaturedImage {
  originalSrc: string;
}

export interface MixEntity {
  _id: string;
  handle: string;
  title: string;
  variants?: VariantsEntityMix[] | null;
}

export interface VariantsEntityMix {
  id: number;
  image: ImageOrFeaturedImage;
  quantity: number;
  title: string;
}
export interface GroupVariant {
  id: number;
  title: string;
  price: number;
  available: boolean;
  unit: number;
  title2: string,
  pricePerUnit: string
  hide: boolean
}
