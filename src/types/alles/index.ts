import { IVariant } from 'Types/shopify/common.type';
import { IProduct } from 'Types/shopify/product.type';

export type TSortType = 'product-type' | 'lowest-price' | 'highest-price';

/**
 * Variants như của Shopify, nhưng có thêm property variant_image từ metafields
 */
export interface IVariantCustom extends IVariant {
  variant_image: string
}

/**
 * Product có thêm 1 vải property mới
 */
export interface IProductCustom extends IProduct{
  variants: IVariantCustom[]
}

/**
 * Collection chứa nhưững product có thêm property so với nguyên gốc Shopify
 */
export interface ICollectionCustom {
  title: string,
  products: IProduct[],
}

export interface IToastMessage {
  title: string,
  quantity: string,
  decscription: string,
}
