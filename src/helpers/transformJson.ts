import { VariantsEntity } from 'Types/product-information/information';

/**
 * Update ngày 31/05/2021
 * Một số json từ app được đổi dẫn đến 1 vài components có thể bị đổi logic theo.
 * Hàm này giúp convert từ json mới ( chứa groups ) của các Variants ra thành json cũ
 *
  interface VariantsEntity {
    ingredientInformations: IngredientInformations;
    nutritionInformations: NutritionInformations;
    image: ImageOrFeaturedImage;
    media: Media[];
    groups: IGroupItem[];
  }
 *
 * =>
 *
 *
 export interface VariantsEntity {
    available: boolean;
    quantity: number;
    id: number;
    ingredientInformations: IngredientInformations;
    nutritionInformations: NutritionInformations;
    price: string;
    image: ImageOrFeaturedImage;
    title: string;
    media: Media[];
  }
 *
 *
 * @param variants - Mảng các json variant cũ
 * @returns
 */
const transformVariant = (variants: VariantsEntity[]): VariantsEntity[] => {
  const newVariants: VariantsEntity[] = [];
  variants.forEach((variant) => {
    let newVariant = {} as VariantsEntity;
    variant.groups.forEach((groupItem) => {
      newVariant.id = groupItem.id;
      newVariant.title = groupItem.title;
      newVariant.image = variant.image;
      newVariant.price = variant.price;
      newVariant.quantity = variant.quantity;
      newVariant.featured_image = variant?.media[0]?.image?.originalSrc || '';

      newVariants.push(newVariant);
      newVariant = {} as VariantsEntity;
    });
  });

  return newVariants;
};

export { transformVariant };
