// import './swiper';

/* Tinh chieu dai cua man hinh
* ( 100vh - 2 menu tren duoi tren 1 so browser)
*/
const screenHeight = (): void => {
  const doc = document.documentElement;
  doc.style.setProperty('--screen-height', `${window.innerHeight}px`);
};

window.addEventListener('resize', screenHeight);
screenHeight();
