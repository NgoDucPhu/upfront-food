import Swiper, { Autoplay, Navigation } from 'swiper';
import Vue from 'vue';
import Component from 'vue-class-component';
import { onSectionSelected } from 'Helpers/sections';

@Component
class HeaderSliderBar extends Vue {
  containerEl = '#slider-bar';

  mounted(): void {
    Swiper.use([Navigation, Autoplay]);
    const headerSliderBar = new Swiper(`${this.containerEl} .swiper-container`, {
      loop: true,
      autoplay: {
        delay: 2500,
        disableOnInteraction: false,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

  }
}

onSectionSelected('#slider-bar', () => {
  const productSlide = new HeaderSliderBar({
    el: '#slider-bar',
    delimiters: ['${', '}'],
  });
});
