import Vue from 'vue';
import Component from 'vue-class-component';
import Swiper from 'swiper';
import { onSectionSelected } from 'Helpers/sections';

@Component
class CollectionMenu extends Vue {
  containerEl: '#collectionMenu';

  path: string = window.location.href;

  mounted(): void {
    const screenWidth = window.innerWidth
      || document.documentElement.clientWidth
      || document.body.clientWidth;

    if (screenWidth < 756) {
      const collectionMenuSwiper = new Swiper('#collectionMenu', {
        slidesPerView: 'auto',
      });
    }
  }

  get onPagesOrCollection(): boolean {
    if (this.path.includes('/pages') || this.path.includes('/collections')) {
      return true;
    }
    return false;
  }
}

onSectionSelected('#collectionMenu', () => {
  const collectionMenu = new CollectionMenu({
    el: '#collectionMenu',
    delimiters: ['${', '}'],
  });
});
