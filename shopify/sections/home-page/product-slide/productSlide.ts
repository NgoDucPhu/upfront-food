import Swiper, { Navigation, Scrollbar, Mousewheel } from 'swiper';
import Vue from 'vue';
import Component from 'vue-class-component';
import { mapActions, mapGetters } from 'vuex';
import store from 'Vue/store';
import { onSectionSelected } from 'Helpers/sections';
import ProductItem from 'Vue/components/globals/ProductItem.vue';
import { IProductCustom } from 'Types/alles';

/**
 * Description:
 * allProducts: biến global chứa các json product lặp từ liquid, trong đó có thêm thuộc tính
 *              variant_image ( lây từ metafield, shopify mặc định không có)
 */
declare let allProducts: IProductCustom;

@Component({
  store,
  computed: {
    ...mapGetters('CollectionStore', {
      isCollectionLoading: 'getIsCollectionLoading',
    }),
  },
  methods: {
    ...mapActions('CollectionStore', [
      'onDisabledLoading',
    ]),
  },
  components: {
    ProductItem,
  },
})
class ProductSlide extends Vue {
  isCollectionLoading!: boolean;

  onDisabledLoading!: () => void;

  allProducts: IProductCustom = allProducts;

  containerEl = '#productSlideContainer';

  mounted(): void {
    Swiper.use([Navigation, Scrollbar, Mousewheel]);
    const productSlide = new Swiper(`${this.containerEl} .swiper-container`, {
      slidesPerView: 'auto',
      // simulateTouch: false,
      threshold: 8,
      mousewheel: {
        forceToAxis: true,
        invert: false,
      },
      scrollbar: {
        el: `${this.containerEl} .swiper-scrollbar`,
        draggable: true,
      },
      navigation: {
        nextEl: `${this.containerEl} .swiper-button-next`,
        prevEl: `${this.containerEl} .swiper-button-prev`,
      },
    });

    const containerEl = productSlide.$el[0] as HTMLElement;
    containerEl.style.overflow = 'visible';
    containerEl.style.width = '100%';
  }
}

onSectionSelected('#productSlideContainer', () => {
  const productSlide = new ProductSlide({
    el: '#productSlideContainer',
    delimiters: ['${', '}'],
  });
});
