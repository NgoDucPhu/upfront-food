/* eslint-disable @typescript-eslint/no-use-before-define */
import Vue from 'vue';
import Component from 'vue-class-component';
import { mapActions } from 'vuex';
import store from 'Vue/store';
import { qsa, qs } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections';

const editForm = ():void => {
  const modal = qsa('.modal-address') as NodeListOf<HTMLElement>;
  const editBtn = qsa('.address__item-edit') as NodeListOf<HTMLElement>;
  editBtn.forEach((item, index) => {
    item.addEventListener('click', () => {
      modal[index + 1].classList.add('modal-address-edit--is-active');
      // document.body.classList.toggle('remove-scrolling');
    });
  });
};

const closeForm = ():void => {
  const close = qsa('.modal-address__icon') as NodeListOf<HTMLElement>;
  close.forEach((item) => {
    item.addEventListener('click', () => {
      clear();
    });
  });
  const overlay = qs('.address-overlay') as HTMLElement;
  overlay.addEventListener('click', () => {
    clear();
  });
};

const clear = ():void => {
  const modal = qsa('.modal-address') as NodeListOf<HTMLElement>;
  modal.forEach((item) => {
    item.classList.remove('modal-address-edit--is-active');
  });
  document.body.classList.remove('remove-scrolling');
};

@Component({
  store,
  methods: {
    ...mapActions('SearchStore', [
      'toggleSearchBar',
    ]),
  },
})
class Address extends Vue {
  isExpand = false;

  toggleExpand(): void {
    this.isExpand = !this.isExpand;
  }

  mounted(): void {
    editForm();
    closeForm();
  }
}

onSectionSelected('#address', () => {
  const address = new Address({
    el: '#address',
    delimiters: ['${', '}'],
  });
});
