import { qs, qsa } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections';

class Footer {
  title: NodeListOf<HTMLElement>;

  listItem: NodeListOf<HTMLElement>;

  constructor() {
    this.title = qsa('.footer__title') as NodeListOf<HTMLElement>;
    this.listItem = qsa('.footer__menu-list-desktop') as NodeListOf<HTMLElement>;
  }

  init(): void {
    this.click();
  }

  click(): void {
    this.title.forEach((item: HTMLElement, index: any) => {
      item.addEventListener('click', () => {
        const ele = this.listItem[index] as HTMLElement;
        const maxHeight = ele.scrollHeight;
        ele.style.setProperty('--max-height-footer', `${maxHeight}px`);
        ele.parentElement?.classList.toggle('is-expand');
      });
    });
  }
}

onSectionSelected('.footer', () => {
  const footer = new Footer();
  footer.init();
});
