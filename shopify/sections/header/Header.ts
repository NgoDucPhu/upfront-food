import { Vue, Watch, Component } from 'vue-property-decorator';
import { Debounce } from 'vue-debounce-decorator';
import PredictiveSearch from '@shopify/theme-predictive-search';
import CartItemCount from 'Vue/components/globals/CartItemCount.vue';
import { qsa, qs } from 'Helpers/dom/dom';
import { IProductSearch } from 'Types/shopify/product.type';

declare let templateName: any;

type TDataResponse = {
  resources: {
    results?: {
      products: IProductSearch[];
    };
  };
};

/**
 * Handle open, collapsed with menu item nested
 */
const handleToggleNestedMenu = (): void => {
  const expandNestedIconEl = qsa('.menu__list-icon') as NodeListOf<HTMLElement>;
  expandNestedIconEl.forEach((item: HTMLElement, index: number) => {
    item.addEventListener('click', () => {
      qsa('.menu--nested')[index].classList.toggle('is-expand');
    });
  });
};

const getHomepage = (): boolean => {
  if (typeof templateName !== 'undefined' && templateName === 'homepage') {
    return true;
  }

  return false;
};

const handleScrollNav = ():void => {
  const navbar = qs('#shopify-section-header') as HTMLElement;
  const sticky = navbar.offsetTop;

  const myFunction = ():void => {
    if (window.pageYOffset >= sticky) {
      navbar.classList.add('nav-sticky');
    } else {
      navbar.classList.remove('nav-sticky');
    }

    if (qs('#shopify-section-collection-menu')) {
      const collection = qs('#shopify-section-collection-menu') as HTMLElement;
      if (window.pageYOffset >= sticky) {
        getHomepage() && collection.classList.add('collection-sticky');
      } else {
        getHomepage() && collection.classList.remove('collection-sticky');
      }
    }
  };
  window.onscroll = ():void => {
    myFunction();
  };
};

@Component({
  components: {
    CartItemCount,
  },
})
class Header extends Vue {
  isExpand = false;

  products: IProductSearch[] = [];

  number = 0;

  isSearchOpening = false;

  inputText = '';

  toggleSearchBar(): void {
    if (qs('form[action="/search"] input[name="q"]')) {
      const searchInput = qs('form[action="/search"] input[name="q"]') as HTMLElement;
      searchInput.focus();
    }
    this.isSearchOpening = !this.isSearchOpening;
    document.body.classList.toggle('remove-scrolling');
  }

  toggleExpand(): void {
    this.isExpand = !this.isExpand;
  }

  @Debounce(300)
  handleResult(ele: any): void {
    if (!ele.trim()) {
      this.products = [];
    } else {
      const predictiveSearch = new PredictiveSearch({
        resources: {
          type: [PredictiveSearch.TYPES.PRODUCT],
          limit: 10,
          options: {
            unavailable_products: PredictiveSearch.UNAVAILABLE_PRODUCTS.LAST,
            fields: [
              PredictiveSearch.FIELDS.TITLE,
              PredictiveSearch.FIELDS.VENDOR,
              PredictiveSearch.FIELDS.PRODUCT_TYPE,
              PredictiveSearch.FIELDS.VARIANTS_TITLE,
            ],
          },
        },
      });

      predictiveSearch.on('success', (suggestions: TDataResponse) => {
        const productSuggestions = suggestions.resources.results?.products as IProductSearch[];
        this.number = productSuggestions.length;

        if (productSuggestions.length > 0) {
          this.products = productSuggestions;
        } else {
          this.products = [];
        }
      });
      predictiveSearch.on('error', (error: Error) => {
        console.error('Error message:', error.message);
      });
      predictiveSearch.query(ele);
    }
  }

  onClickPopular(e: any): void {
    this.inputText = e.target.innerText;
  }

  @Watch('inputText')
  onSearching(newValue: any, oldValue: any): void {
    this.handleResult(newValue);
  }

  mounted(): void {
    handleToggleNestedMenu();
    handleScrollNav();
  }
}

const header = new Header({
  el: '#xo-header',
  delimiters: ['${', '}'],
});
