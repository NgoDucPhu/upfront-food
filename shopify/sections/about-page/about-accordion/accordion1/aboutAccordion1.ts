import { qsa } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections';

class AboutAccordion {
  accordionTitleList: NodeListOf<HTMLElement>;

  accordionDescList: NodeListOf<HTMLElement>;

  accordionItems: NodeListOf<HTMLElement>;

  constructor() {
    this.accordionItems = qsa('.about-accordion__item') as NodeListOf<HTMLElement>;
    this.accordionTitleList = qsa('.about-accordion__title') as NodeListOf<HTMLElement>;
    this.accordionDescList = qsa('.about-accordion__desc') as NodeListOf<HTMLElement>;
  }

  init(): void {
    this.handleExpand();
    this.handleHeight();
  }

  handleExpand(): void {
    this.accordionTitleList.forEach((item: HTMLElement, index) => {
      item.addEventListener('click', () => {
        const ele = this.accordionDescList[index] as HTMLElement;
        const maxHeight = ele.scrollHeight;
        ele.style.setProperty('--max-height-desc', `${maxHeight}px`);
        ele.parentElement?.classList.toggle('is-expand');
      });
    });
  }

  handleHeight(): void {
    const scrollHeightArr = Array.from(this.accordionItems).map((accItem: HTMLElement) => accItem.scrollHeight);
    const height = scrollHeightArr.reduce((acc, val): number => (acc > val ? acc : val), 0);
    this.accordionTitleList.forEach((accItem: HTMLElement) => {
      accItem.style.setProperty('--height-title', `${height}px`);
    });
  }
}
onSectionSelected('#aboutInforContainer1', () => {
  const aboutAccordion = new AboutAccordion();
  aboutAccordion.init();
});
