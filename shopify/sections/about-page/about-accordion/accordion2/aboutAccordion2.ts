import { qs, qsa } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections';

const nextUntil = (elem: HTMLElement, selector: string, wrapperElement: HTMLElement): HTMLElement => {
  const siblings = [];
  elem = elem.nextElementSibling as HTMLElement;

  while (elem) {
    if (elem.matches(selector)) break;
    siblings.push(elem);
    elem = elem.nextElementSibling as HTMLElement;
  }
  wrapperElement.append(...siblings);

  return wrapperElement;
};

class AboutAccordion {
  accordionContainer: NodeListOf<HTMLElement>;

  constructor() {
    this.accordionContainer = qsa('#accordionContainer h2') as NodeListOf<HTMLElement>;
  }

  init(): void {
    this.handleAccordion();
  }

  handleAccordion(): void {
    this.accordionContainer?.forEach((h2Element, i) => {
      h2Element.classList.add('about-accordion2__title');
      const descEl = document.createElement('div');
      descEl.classList.add('about-accordion2__desc');
      const wrapperAccordion = nextUntil(h2Element, 'h2', document.createElement('div'));
      descEl.appendChild(wrapperAccordion);
      h2Element.insertAdjacentElement('afterend', descEl);

      h2Element.addEventListener('click', () => {
        const maxHeight = descEl.scrollHeight;
        const h2NextEleSibling = h2Element.nextElementSibling as HTMLElement;
        h2NextEleSibling.style.setProperty('--max-height-desc2', `${maxHeight}px`);
        h2Element.parentElement?.classList.toggle('is-expand');
      });

      const accordionItemEl = document.createElement('div');
      accordionItemEl.classList.add('about-accordion2__item');
      h2Element.insertAdjacentElement('beforebegin', accordionItemEl);
      accordionItemEl.append(...[h2Element, descEl]);
    });
  }
}

onSectionSelected('#aboutInforContainer2', () => {
  const aboutAccordion = new AboutAccordion();
  aboutAccordion.init();
});
