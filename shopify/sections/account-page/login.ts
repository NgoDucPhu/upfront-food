import { qs } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections';

class Login {
  loginConatiner: HTMLElement;

  recoverForm: HTMLElement;

  loginForm: HTMLElement;

  constructor() {
    this.loginConatiner = qs('.login-container') as HTMLElement;
    this.loginForm = qs('.xo-login-form span') as HTMLElement;
    this.recoverForm = qs('.xo-recover-form') as HTMLElement;
  }

  init(): void {
    this.toggleForm();
  }

  toggleForm(): void {
    this.loginForm.addEventListener('click', () => {
      this.loginConatiner.classList.remove('is-active');
    });

    this.recoverForm.addEventListener('click', () => {
      this.loginConatiner.classList.add('is-active');
    });
  }
}

onSectionSelected('#loginWrapper', () => {
  const login = new Login();
  login.init();
});
