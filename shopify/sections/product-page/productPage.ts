import { MixVariant } from './../../../src/types/product-information/information.d';
/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Component from 'vue-class-component';
import { mapActions, mapGetters } from 'vuex';
import store from 'Vue/store/index';
import AddToCartBtn from 'Vue/components/globals/AddToCartBtn.vue';
import ItemInfoProduct from 'Vue/components/globals/ItemInfoProduct.vue';
import StarVoting from 'Vue/components/globals/StarVoting.vue';
import ProductImage from 'Vue/components/entry/product/ProductImage.vue';
import ProductDescription from 'Vue/components/entry/product/ProductDescription.vue';
import BloodSugarChart from 'Vue/components/entry/product/Chart.vue';
import { getCookie, setCookie, updateTime } from 'Helpers/utils';
import { onSectionSelected } from 'Helpers/sections';
import type {
  Finances,
  GeneralInformations,
  GroupVariant,
  IngredientInformations,
  Media,
  MixEntity,
  NutritionInformations,
  NutritionsEntity,
  Packages,
  ProductInformation,
  Progress,
  VariantsEntity,
  Description,
  GoedOmTeWetenInfomations,
  Accordion,
  BloodSugar,
} from 'Types/product-information/information';
import { Bottomline } from 'Types/review';
import { qs } from 'Helpers/dom/dom';
import { IProduct } from 'Types/shopify/product.type';
import { getParameterByName, setParam } from 'Helpers/url/url';
/**
 * Set height to scroll
 */
declare let productId: number;
declare let product: IProduct;
@Component({
  store,
  components: {
    AddToCartBtn,
    ItemInfoProduct,
    StarVoting,
    ProductImage,
    ProductDescription,
    BloodSugarChart,
  },
  computed: {
    ...mapGetters('ReviewStore', {
      bottomLine: 'getBottomLine',
    }),
  },
  methods: {
    ...mapActions('ReviewStore', [
      'updateProductId',
      'updateBottomLine',
    ]),
  },
})

class ProductSwiper extends Vue {
  private readonly bottomLine!: Bottomline;

  private productInformation: ProductInformation = {} as ProductInformation;

  private value = 1;

  private variants: VariantsEntity[] = [] as VariantsEntity[];

  private groupVariantSelected: VariantsEntity = { groups: [] as GroupVariant[] } as VariantsEntity;

  private hoverVariant: VariantsEntity = {
    groups: [] as GroupVariant[],
    nutritionInformations: {} as NutritionInformations,
    ingredientInformations: {} as IngredientInformations,
    nutritionsInfo: {} as NutritionInformations,
  } as VariantsEntity;

  private hoverMixVariant: MixVariant = {} as MixVariant;

  private generalInformations: GeneralInformations = {} as GeneralInformations;

  private nutritionSelected: NutritionsEntity = {} as NutritionsEntity;

  private isPopupFirstAccess = false;

  private isFirstAccess = false;

  public loaded = false;

  updateProductId!: (productId: number) => void;

  updateBottomLine!: (productId: number) => void;

  created(): void {
    this.fetchProductInformation();
    this.updateProductId(productId);
    this.updateBottomLine(productId);
  }

  mounted(): void {
    this.isFirstAccess = !getCookie('firstAccess');

    /** Scroll den product review khi click vao vote */
    const productRate = qs('.product__review');
    const productReview = qs('.xo-scroll');
    productRate?.addEventListener('click', () => {
      productReview?.scrollIntoView({ block: 'start', behavior: 'smooth' });
    });
  }

  async fetchProductInformation(): Promise<void> {
    try {
      const fetchUrl = (process.env.IS_UPFRONT_SITE === 'true')
        ? `https://cdn.xopify.com/custom-app/upfrontreep/${productId}.json?${updateTime()}`
        : 'https://cdn.xopify.com/custom-app/upfrontreep/6597120753726.json?1637209666535';

      const res = await fetch(fetchUrl);
      this.productInformation = await res.json();
      if (res.status === 200) {
        this.variants = this.productInformation.variants;
        this.generalInformations = this.productInformation.generalInformations;
        this.updateVariantAvailable();
        this.updateAllVariant();

        /** Chon variant neu co o param URL */
        const urlParams = new URLSearchParams(window.location.search);
        const variantId = Number(urlParams.get('variant'));

        if (variantId) {
          this.variants.forEach((variant) => {
            variant.groups.forEach((grp, index) => {
              if (grp.id === variantId) {
                this.updateVariantGroup(variant, index);
                this.groupVariantSelected = variant;

                setParam('variant', variantId);
              }
            });
          });
        } else {
          /** Chon variant group theo default */
          this.variants.forEach((variant) => {
            if (variant.defaultProductPage) {
              variant.groups.forEach((grp, index) => {
                if (grp.id === variant.defaultProductPage) {
                  this.updateVariantGroup(variant, index);
                }
              });
            }
          });
          [this.groupVariantSelected] = this.variants;
        }

        this.hoverVariant = this.groupVariantSelected;
        this.setNutritionSelected();
        this.loaded = true;
      } else throw new Error();
    } catch (error) {
      console.log(error);
    }
  }

  get isOnlyVariant(): boolean {
    let countVariant = 0;
    this.variants.forEach((variant) => {
      variant.groups.forEach(() => {
        countVariant += 1;
      });
    });
    return countVariant <= 1;
  }

  get isOnlyGroup(): boolean {
    return this.variants.length === 1;
  }

  get quantityVariant(): string {
    const quantityVariant = this.handleQuantity(this.hoverVariant.title);
    return quantityVariant;
  }

  get groups(): GroupVariant[] {
    return this.hoverVariant.groups;
  }

  get metaVariant(): string {
    return this.getMetaVariant(this.hoverVariant);
  }

  get isMix(): boolean {
    return Number(this.productInformation.mix?.length) > 0;
  }

  get nutritionInformation(): NutritionInformations {
    this.groupVariantSelected.nutritionsInfo && this.groupVariantSelected.nutritionsInfo.nutritions.forEach((nutri) => {
      nutri.nutritionalDetail = nutri.nutritionalDetail.sort((x, y) => Number(y.value) - Number(x.value));
    });
    return this.groupVariantSelected.nutritionsInfo;
  }

  get weightNutrition(): number {
    let weight = 0;
    this.nutritionSelected.nutritionalDetail.forEach((nutrition) => {
      if (weight < Number(nutrition.value) && nutrition.type !== 'none') {
        weight = Number(nutrition.value);
      }
      if (weight < Number(nutrition.subNutrition.value) && nutrition.subNutrition.type !== 'none') {
        weight = Number(nutrition.subNutrition.value);
      }
    });
    return weight;
  }

  get bloodSugar(): BloodSugar {
    return this.generalInformations.bloodSugar;
  }

  get chartBloodSugarData(): any {
    const datasets = [] as any;
    this.loaded && this.generalInformations.bloodSugar.items.forEach((item) => {
      // const data = [] as any;
      // item.data.forEach((p) => {
      //   const temp = {} as any;
      //   temp[`${p.x}`] = p.y;
      //   data.push(temp);
      // });
      const dataset = {
        label: item.title.toUpperCase(),
        borderColor: item.color,
        data: item.data,
        pointRadius: 0,
        borderWidth: 2,
        fill: false,
      };
      datasets.push(dataset);
    });

    return { datasets };
  }

  get chartBloodSugarOptions(): any {
    const scales = {
      xAxes: [{
        type: 'linear',
        display: true,
        scaleLabel: {
          display: true,
          labelString: this.loaded && this.generalInformations.bloodSugar.xTitle,
          fontFamily: 'Inconsolata',
          fontSize: 16,
        },
        ticks: {
          fontFamily: 'Inconsolata',
          fontWeight: 600,
        },
      }],
      yAxes: [{
        display: true,
        scaleLabel: {
          display: true,
          labelString: this.loaded && this.generalInformations.bloodSugar.yTitle,
          fontFamily: 'Inconsolata',
        },
      }],
    };

    const legend = {
      display: true,
      position: 'bottom',
      labels: {
        boxWidth: 10,
        fontStyle: 'bold',
        fontFamily: 'Inconsolata',
        fontColor: '#000',
      },
    };

    return { scales, legend };
  }

  get ingredientInformations(): IngredientInformations {
    return this.groupVariantSelected.ingredientInformations;
  }

  get reviewBackground(): string {
    return this.productInformation.reviewBackground || 'https://cdn.shopify.com/s/files/1/0254/4667/8590/t/28/assets/reviews_background.png?v=1623409269';
  }

  get boekBackground(): string {
    return this.productInformation.boekBackground || 'https://cdn.shopify.com/s/files/1/0254/4667/8590/t/28/assets/open-boek2.png?v=1623932203';
  }

  get goedOmTeWetenInfomations(): GoedOmTeWetenInfomations {
    return this.groupVariantSelected.goedOmTeWetenInfomations;
  }

  get accordion(): Accordion[] {
    return this.groupVariantSelected.accordion;
  }

  get packages(): Packages {
    return this.generalInformations.packages;
  }

  get finances(): Finances {
    return this.generalInformations.finances;
  }

  get progress(): Progress {
    return this.generalInformations.progress;
  }

  get descriptions(): Description[] {
    return this.generalInformations.descriptions;
  }

  get mix(): MixEntity[] | null | undefined {
    return this.productInformation.mix;
  }

  get generalMedia(): Media[] {
    return this.productInformation.media;
  }

  public onMouseMoveVariant(variant: VariantsEntity): void {
    this.hoverVariant = variant;
  }

  public onMouseLeaveVariant(): void {
    this.hoverVariant = this.groupVariantSelected;
  }

  public onMouseMoveMixVariant(mixVariant: MixVariant): void {
    this.hoverMixVariant = mixVariant;
  }

  public onMouseLeaveMixVariant(): void {
    this.hoverMixVariant = {} as MixVariant;
  }

  public updateVariant(variant: VariantsEntity): void {
    this.groupVariantSelected = variant;
    this.setNutritionSelected();
  }

  public updateVariantGroup = (variant: VariantsEntity, indexGroup = 0): void => {
    Vue.set(variant, 'id', variant.groups[indexGroup].id);
    Vue.set(variant, 'available', variant.groups[indexGroup].available);
    Vue.set(variant, 'price', variant.groups[indexGroup].price);
    Vue.set(variant, 'title', variant.groups[indexGroup].title);
    Vue.set(variant, 'quantity', variant.groups[indexGroup].unit);
  };

  public updateParam = (variantId: number): void => {
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.set('variant', `${variantId}`);
    window.history.replaceState(null, '', `?${urlParams}`);
  };

  public setNutritionSelected(index = 0): void {
    this.nutritionSelected = this.groupVariantSelected && this.groupVariantSelected.nutritionsInfo.nutritions[index];
  }

  public isHidenAllVariant = (variant: VariantsEntity): boolean => {
    const hidden = variant.groups.every((group) => group.hide);
    return hidden;
  };

  /** Update initial data variant */
  private updateAllVariant(): void {
    this.variants.forEach((variantGroup: VariantsEntity) => {
      this.updateVariantGroup(variantGroup);
    });
  }

  private getMetaVariant = (choosenVariant: VariantsEntity): string => {
    let metaVariant = '';
    if (!this.isMix) {
      const res = (choosenVariant.groups[0] || { title: '' }).title.split(' ');
      if (res.length > 1) {
        (res.slice(0, -1)).forEach((title: string) => {
          metaVariant += `${title} `;
        });
      } else {
        [metaVariant] = res;
      }
    }
    return metaVariant || '';
  };

  public handleFirstAccess(): void {
    this.handlePopup(true);
    setCookie('firstAccess', 'true', 365);
  }

  public handleClickFirstAccess(isNee: boolean): void {
    this.handlePopup(false);

    if (isNee) {
      this.isFirstAccess = false;

      /** expand item dau tien */
      const itemInfo = qs('.product__infor-wrapper .item__label') as HTMLElement;
      itemInfo.click();

      /** Scroll to info product */
      const productInfoEl = qs('.product__infor-wrapper');
      productInfoEl?.scrollIntoView({ block: 'start', behavior: 'smooth' });
    } else {
      /** Tiep tuc mua hang */
      const addToCartBtn = qs('.product__addtocart .btn') as HTMLElement;
      addToCartBtn.click();
    }
  }

  /** Handle value input */
  public handleValue(): void {
    if (!this.value) {
      this.value = 1;
    }
  }

  private sortVariant(): void {
    this.variants = this.variants.reverse();
    this.variants = this.variants.sort((x, y) => Number(x?.groups[0].available) - Number(y?.groups[0].available)).reverse();
  }

  private handlePopup(isShow: boolean): void {
    this.isPopupFirstAccess = isShow;
  }

  /** Get quantity from title */
  private handleQuantity = (title = ' '): string => {
    const quantity = title.split(' ');
    if (quantity.length >= 1) {
      if (!Number.isNaN(Number(quantity[quantity.length - 1]))) {
        const quantityVariant = quantity[quantity.length - 1];
        return quantityVariant;
      }
    }
    return '';
  };

  /** Update variant from liquid */
  private updateVariantAvailable(): void {
    product.variants.forEach((variantLiquid) => {
      this.variants.forEach((variantJson) => {
        variantJson.groups.forEach((variant) => {
          if (variant.id === variantLiquid.id) {
            variant.price = variantLiquid.price;
            variant.available = variantLiquid.available;
          }
        });
      });
    });

    // this.sortVariant();
  }
}
onSectionSelected('#shopify-section-product-page', () => {
  const productSwiper = new ProductSwiper({
    el: '#shopify-section-product-page',
    delimiters: ['${', '}'],
  });
});
