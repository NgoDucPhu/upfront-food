import { ICart, IProperty } from 'Types/shopify/cart.type';
import Vue from 'vue';
import Component from 'vue-class-component';
import { onSectionSelected } from 'Helpers/sections';
import store from 'Vue/store';
import { mapActions, mapGetters } from 'vuex';

declare let theme: any;

@Component({
  store,
  computed: {
    ...mapGetters('CartStore', {
      cartState: 'getCartState',
      cartItemCount: 'getCartItemCount',
    }),
  },
  methods: {
    ...mapActions('CartStore', [
      'updateItemByKey',
    ]),
  },
})

class CartPage extends Vue {
  private readonly cartState!: ICart;

  titleStr: string = theme.cartStr.title;

  productStr: string = theme.cartStr.product;

  priceStr: string = theme.cartStr.price;

  quantityStr: string = theme.cartStr.quantity;

  totalStr: string = theme.cartStr.total;

  subTotalStr: string = theme.cartStr.subTotal;

  continueShoppingStr: string = theme.cartStr.continueShopping;

  taxesAndShippingAtCheckoutStr: string = theme.cartStr.taxesAndShippingAtCheckout;

  checkoutStr: string = theme.cartStr.checkout;

  unavailableStr: string = theme.cartStr.unavailable;

  created():void {
    window.location.href = '/pages/shop-alles';
  }

  updateItemByKey!: (config: {
    key: string,
    quantity?: number,
    properties: IProperty,
    onSuccess?: (cartState: ICart) => void;
    onError?: (err: Error) => void;
  }) => Promise<void>;

  updateQuantity(quantity: number, key: string): void {
    this.updateItemByKey({
      key,
      quantity,
      properties: {},
    });
  }

  removeItem(e: any, quantity: number): void {
    const { key } = e.target.dataset;

    this.updateItemByKey({
      key,
      quantity,
      properties: {},
    });
  }
}

onSectionSelected('#cartContainer', () => {
  const cartPage = new CartPage({
    el: '#cartContainer',
    delimiters: ['${', '}'],
  });
});
