import Vue from 'vue';
import Component from 'vue-class-component';
import { mapActions, mapGetters } from 'vuex';
import store from 'Vue/store';
import ProductItem from 'Vue/components/globals/ProductItem.vue';
import { onSectionSelected } from 'Helpers/sections';
import { IProductCustom } from 'Types/alles';

/**
 * Description:
 * allProducts: biến global chứa các json product lặp từ liquid, trong đó có thêm thuộc tính
 *              variant_image ( lây từ metafield, shopify mặc định không có)
 */
declare let allProducts: IProductCustom;

@Component({
  store,
  computed: {
    ...mapGetters('CollectionStore', {
      isCollectionLoading: 'getIsCollectionLoading',
    }),
  },
  methods: {
    ...mapActions('CollectionStore', [
      'onDisabledLoading',
    ]),
  },
  components: {
    ProductItem,
  },
})
class CollectionTemplate extends Vue {
  isCollectionLoading!: boolean;

  onDisabledLoading!: () => void;

  allProducts: IProductCustom = allProducts;

  mounted(): void {
    this.onDisabledLoading();
  }
}

onSectionSelected('#collectionTemplate', () => {
  const productSlide = new CollectionTemplate({
    el: '#collectionTemplate',
    delimiters: ['${', '}'],
  });
});
