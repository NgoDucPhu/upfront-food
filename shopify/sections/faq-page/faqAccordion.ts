import { qs, qsa } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections';
import Vue from 'vue';
import Component from 'vue-class-component';
import ModalFaq from 'Vue/components/entry/faq/ModalFaq.vue';

interface ICategory {
  item: string;
  value: string;
}

interface ICategoryValue {
  item: HTMLElement;
  value: string[];
}

interface ICategoryObj {
  item: string;
  value: HTMLElement;
}

@Component({
  components: {
    ModalFaq,
  },
})
class FaqAccordion extends Vue {
  faqItems: NodeListOf<HTMLElement> | [];

  faqQuestions: NodeListOf<HTMLElement> | [];

  faqAnswers: NodeListOf<HTMLElement> | [];

  listCategoryValue: (string)[] = [];

  listCategory: (ICategory)[] = [];

  list: (ICategory)[] = [];

  faqSection: HTMLElement;

  faqCategoryLabel: NodeListOf<HTMLElement> | [];

  mounted(): void {
    this.faqItems = qsa('.faq__item') as NodeListOf<HTMLElement>;
    this.faqQuestions = qsa('.faq__question') as NodeListOf<HTMLElement>;
    this.faqAnswers = qsa('.faq__answer') as NodeListOf<HTMLElement>;
    this.faqSection = qs('.faq__item-section') as HTMLElement;
    this.faqCategoryLabel = qsa('.faq-category__label') as NodeListOf<HTMLElement>;
    this.getListCategory();
    this.handleExpand();
    this.wrapperFaq();
  }

  handleExpand(): void {
    this.faqQuestions.forEach((item: HTMLElement, index: any) => {
      item.addEventListener('click', () => {
        const ele = this.faqAnswers[index] as HTMLElement;
        const maxHeight = ele.scrollHeight;
        ele.style.setProperty('--max-height-answer', `${maxHeight}px`);
        ele.parentElement?.classList.toggle('is-expand');
      });
    });
  }

  getListCategory(): (ICategory)[] {
    const faqSelect = qs('.faq__content-select') as HTMLElement;
    const listCate = faqSelect.dataset.cate?.split(',') as (string)[];

    this.faqItems.forEach((item: HTMLElement) => {
      const categoryValue = item.dataset.category?.split(',') as (string)[];
      categoryValue.forEach((ele) => {
        if (ele) {
          this.listCategoryValue.push(ele);
        }
      });
    });

    listCate.forEach((item, index) => {
      this.listCategory.push({
        item,
        value: `category-${index}`,
      });
    });
    this.list = this.listCategory.filter((item) => this.listCategoryValue.includes(item.value));
    return this.list;
  }

  // eslint-disable-next-line class-methods-use-this
  onFilter(value: any): void {
    const categoryLabel = qsa('.faq-category__label') as NodeListOf<HTMLElement>;
    categoryLabel.forEach((item: HTMLElement) => {
      // eslint-disable-next-line no-param-reassign
      item.style.display = 'none';
    });

    this.clearFaqItem();
    this.faqItems.forEach((item: HTMLElement, index: number) => {
      const itemDataset = item.dataset.category?.split(',') as (string)[];
      itemDataset.forEach((ele) => {
        if (ele && ele === value) {
          this.faqItems[index].style.display = 'flex';
        }
      });
    });

    if (value === 'category-0') {
      this.filerCategoryLabel();
    }
  }

  // eslint-disable-next-line class-methods-use-this
  clearFaqItem(): void {
    this.faqItems.forEach((ele: HTMLElement) => {
      // eslint-disable-next-line no-param-reassign
      ele.style.display = 'none';
    });
  }

  wrapperFaq(): void {
    // this.clearFaqItem();
    const arrTemp: (ICategoryValue)[] = [];

    this.faqItems.forEach((item: HTMLElement) => {
      const temp: string[] = [];
      const itemDataset = item.dataset.category?.split(',');
      itemDataset?.forEach((ele, index) => {
        if (index > 0 && ele) {
          temp.push(ele);
          arrTemp.push({
            item,
            value: temp,
          });
        }
      });
    });

    const listLabel = this.list.slice(1);
    const temp: ICategoryObj[] = [];

    listLabel.forEach((item) => {
      arrTemp.forEach((ele) => {
        if (ele.value.indexOf(item.value) > -1) {
          temp.push({
            item: item.item,
            value: ele.item,
          });
        }
      });
    });

    temp.forEach((item) => {
      const h3Ele = document.createElement('h3');
      h3Ele.classList.add('faq-category__label');
      h3Ele.setAttribute('data-category-label', `${item.item}`);
      h3Ele.innerHTML = `${item.item}`;
      item.value.insertAdjacentElement('beforebegin', h3Ele);
      this.faqSection.append(...[h3Ele, item.value]);
    });

    this.filerCategoryLabel();
  }

  // eslint-disable-next-line class-methods-use-this
  filerCategoryLabel(): void {
    const categoryLabel = qsa('.faq-category__label') as NodeListOf<HTMLElement>;
    const h3List: ICategoryObj[] = [];
    categoryLabel.forEach((item: HTMLElement) => {
      // eslint-disable-next-line no-param-reassign
      item.style.display = 'none';
      const itemDataset = item.dataset.categoryLabel as string;
      h3List.push({
        item: itemDataset,
        value: item,
      });
    });

    const filterArray = h3List.reduce((accumalator: ICategoryObj[], current) => {
      if (!accumalator.some((item: ICategoryObj) => item.item === current.item)) {
        accumalator.push(current);
      }
      return accumalator;
    }, []);

    filterArray.forEach((item) => {
      // eslint-disable-next-line no-param-reassign
      item.value.style.display = 'block';
    });
  }
}

onSectionSelected('#faqContainer', () => {
  const faqAccordion = new FaqAccordion({
    el: '#faqContainer',
    delimiters: ['${', '}'],
  });
});
