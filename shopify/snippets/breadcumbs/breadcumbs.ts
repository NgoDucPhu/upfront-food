import { qsa } from 'Helpers/dom/dom';
import { onSectionSelected } from 'Helpers/sections';

class Breadcumbs {
  collections: NodeListOf<HTMLElement>;

  constructor() {
    this.collections = qsa('.breadcrumbs .breadcrumbs-collection-id') as NodeListOf<HTMLElement>;
  }

  init(): void {
    this.showCollection();
  }

  showCollection(): void {
    const allCollections = ['273126359232', '278829596864', '273369202880', '273369268416', '275705102528', '27336821984'];

    const results: { collectionId: string; collectionPosition: number; }[] = [];
    const productCollections: any[] = [];

    this.collections.forEach((item: HTMLElement) => {
      productCollections.push(item.dataset.id);
    });

    allCollections.forEach((allCollection) => {
      productCollections.forEach((productCollection, index) => {
        if (allCollection === productCollection) {
          results.push({
            collectionId: allCollection,
            collectionPosition: index,
          });
        }
      });
    });

    if (results.length) this.collections[results[0].collectionPosition].classList.add('show');
  }
}
onSectionSelected('.breadcrumbs', () => {
  const breadcumbs = new Breadcumbs();
  breadcumbs.init();
});
